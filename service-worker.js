const timestamp = 1638510278687;
const build = [
  "/_app/start-6a0f4d70.js",
  "/_app/assets/start-808c0b29.css",
  "/_app/pages/__layout.svelte-0e8766a9.js",
  "/_app/assets/pages/__layout.svelte-5aa727c7.css",
  "/_app/error.svelte-d94167e5.js",
  "/_app/pages/index.svelte-6754a807.js",
  "/_app/assets/pages/index.svelte-3a70660a.css",
  "/_app/pages/portfolio.svelte-62c8c58a.js",
  "/_app/pages/services.svelte-7ba2751b.js",
  "/_app/assets/pages/services.svelte-2e513cab.css",
  "/_app/pages/aboutus.svelte-c1f5a607.js",
  "/_app/pages/contact.svelte-4a7ee1bf.js",
  "/_app/pages/tariffs.svelte-eee752ae.js",
  "/_app/pages/diffs.svelte-f1653321.js",
  "/_app/pages/windi.svelte-c36a8d36.js",
  "/_app/pages/auth/signin.svelte-ad7054ec.js",
  "/_app/pages/auth/signup.svelte-9d801002.js",
  "/_app/pages/test.svelte-2bb4d31f.js",
  "/_app/chunks/vendor-4df61e8f.js",
  "/_app/assets/vendor-886ef551.css",
  "/_app/chunks/preload-helper-ec9aa979.js",
  "/_app/chunks/Container-72d89736.js",
  "/_app/chunks/Section-4e911efb.js",
  "/_app/assets/Section-a9cce6d2.css",
  "/_app/chunks/Swipe-2d7d67db.js",
  "/_app/assets/Swipe-bed73967.css",
  "/_app/chunks/SwipeItem-0743ba43.js",
  "/_app/assets/SwipeItem-714a3cb2.css",
  "/_app/chunks/loopTypewriter-2a163f9d.js",
  "/_app/chunks/getRandomElement-20df41a3.js",
  "/_app/chunks/writeEffect-ad25c0cc.js",
  "/_app/chunks/typingInterval-b805e32c.js",
  "/_app/chunks/scramble-fa1afc2b.js",
  "/_app/chunks/isInRange-d476866c.js",
  "/_app/chunks/typewriter-fb050f81.js",
  "/_app/chunks/onAnimationEnd-c4ef4008.js",
  "/_app/chunks/index-e3190056.js"
];
const files = [
  "/.DS_Store",
  "/css/font-awesome.css",
  "/css/montserrat.css",
  "/css/rolly.css",
  "/css/svg-with-js.min.css",
  "/favicon.ico",
  "/fonts/FontAwesome/fa-brands-400.woff2",
  "/fonts/FontAwesome/fa-duotone-900.woff2",
  "/fonts/FontAwesome/fa-light-300.woff2",
  "/fonts/FontAwesome/fa-regular-400.woff2",
  "/fonts/FontAwesome/fa-solid-900.woff2",
  "/fonts/FontAwesome/fa-thin-100.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-100.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-100.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-200.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-200.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-300.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-300.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-500.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-500.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-600.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-600.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-700.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-700.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-800.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-800.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-900.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-900.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-italic.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-italic.woff2",
  "/fonts/Montserrat/montserrat-v14-cyrillic-regular.woff",
  "/fonts/Montserrat/montserrat-v14-cyrillic-regular.woff2",
  "/images/.DS_Store",
  "/images/logo-vector.svg",
  "/images/main/.DS_Store",
  "/images/main/fixed-fast.jpg",
  "/images/main/fixed-lazy.jpg",
  "/images/main/headerInfo/comp.png",
  "/images/main/headerInfo/icons/1.png",
  "/images/main/headerInfo/icons/2.png",
  "/images/main/headerInfo/icons/3.png",
  "/images/main/headerInfo/icons/4.png",
  "/images/main/hero-fast.jpg",
  "/images/main/hero-lazy.jpg",
  "/images/main/howWeCanHelp/how-we-can-help.png",
  "/images/main/reasonsWhyUs/icons/handshake.png",
  "/images/main/reasonsWhyUs/icons/quality.png",
  "/images/main/reasonsWhyUs/icons/speed.png",
  "/images/main/reasonsWhyUs/icons/warranty.png",
  "/images/services/computer-bg.png",
  "/images/services/header-fast.jpg",
  "/images/services/header-lazy.jpg",
  "/images/tariffs/header-fast.jpg",
  "/images/tariffs/header-lazy.jpg",
  "/images/themesdev-logo-dark.svg",
  "/images/user.jpg",
  "/js/fontawesome/brands.js",
  "/js/fontawesome/fontawesome.js",
  "/js/fontawesome/light.js",
  "/js/fontawesome/regular.js",
  "/js/fontawesome/solid.js",
  "/logo_512.png",
  "/manifest.json",
  "/robots.txt"
];
const ASSETS = `cache${timestamp}`;
const to_cache = build.concat(files);
const staticAssets = new Set(to_cache);
self.addEventListener("install", (event) => {
  event.waitUntil(caches.open(ASSETS).then((cache) => cache.addAll(to_cache)).then(() => {
    self.skipWaiting();
  }));
});
self.addEventListener("activate", (event) => {
  event.waitUntil(caches.keys().then(async (keys) => {
    for (const key of keys) {
      if (key !== ASSETS)
        await caches.delete(key);
    }
    self.clients.claim();
  }));
});
async function fetchAndCache(request) {
  const cache = await caches.open(`offline${timestamp}`);
  try {
    const response = await fetch(request);
    cache.put(request, response.clone());
    return response;
  } catch (err) {
    const response = await cache.match(request);
    if (response)
      return response;
    throw err;
  }
}
self.addEventListener("fetch", (event) => {
  if (event.request.method !== "GET" || event.request.headers.has("range"))
    return;
  const url = new URL(event.request.url);
  const isHttp = url.protocol.startsWith("http");
  const isDevServerRequest = url.hostname === self.location.hostname && url.port !== self.location.port;
  const isStaticAsset = url.host === self.location.host && staticAssets.has(url.pathname);
  const skipBecauseUncached = event.request.cache === "only-if-cached" && !isStaticAsset;
  if (isHttp && !isDevServerRequest && !skipBecauseUncached) {
    event.respondWith((async () => {
      const cachedAsset = isStaticAsset && await caches.match(event.request);
      return cachedAsset || fetchAndCache(event.request);
    })());
  }
});
